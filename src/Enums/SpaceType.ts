const SpaceTypes = [
    {
      value: 'House',
      label: 'House',
    },
    {
      value: 'Apartment',
      label: 'Apartment',
    },
    {
      value: 'Studio',
      label: 'Studio',
    },
  ];

export default SpaceTypes;