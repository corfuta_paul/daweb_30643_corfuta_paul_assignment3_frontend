const ActionTypes = [
    {
      value: 'Rent',
      label: 'Rent',
    },
    {
      value: 'Sale',
      label: 'Sale',
    },
  ];
export default ActionTypes;