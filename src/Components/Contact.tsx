import { Card, CardActionArea, CardContent, CardMedia, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

const Contact: React.FunctionComponent<{}> = () => {
    const { t } = useTranslation();
    return <div>
        <h1 style={{ textAlign: "center" }}>{t('contact')}</h1>
        <Card sx={{ maxWidth: 600 }} className="center">
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="600"
                    image="/images/contact.jpg"
                    alt="green iguana"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {t('contact-us')}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        <ul style={{ textAlign: "left" }}>
                            <li>
                                {t('phone')}: +40753994388
                            </li>
                            <li>
                                {t('address')}: Cluj-Napoca, str.Observator, nr.34
                            </li>
                            <li>
                                Email: agentie_imobiliare@gmail.com
                            </li>
                        </ul>
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    </div>
}

export default Contact;