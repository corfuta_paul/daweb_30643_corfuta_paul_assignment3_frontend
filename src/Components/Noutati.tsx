import { Button, Card, CardActionArea, CardActions, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import xml_data from "./News";

interface News {
    id:number;
    title: string;
    text: string;
    image: string;
}

const Noutati: React.FunctionComponent<{}> = () => {

    const { t } = useTranslation();
    const [list, setList] = useState<News[]>([]);
    const list1:News[] = [];

    var i: number;

    useEffect(() => {
        const parser = new DOMParser();
        const xml = parser.parseFromString(xml_data, 'text/xml');
        

        for (i = 0; i < xml.getElementsByTagName("New").length; i++) {
            var t: string = xml.getElementsByTagName("New")[i].getElementsByTagName("Title")[0].textContent!;
            var n: string = xml.getElementsByTagName("New")[i].getElementsByTagName("Text")[0].textContent!;
            var p: string = xml.getElementsByTagName("New")[i].getElementsByTagName("Photo")[0].textContent!;
            var news: News = {
                id:i, title: t, text: n, image: p
            };
            console.log(news);
            list1.push(news);
        }

        setList(list1);  
    }, [])

   

    return <div>
        <h1 style={{ textAlign: "center" }}>{t('news')}</h1>
        <div style={{ padding: "40px 0px 40px 140px" }}>
        <Grid
            container
            direction="row"
            rowSpacing={10}
            columnSpacing={10}
            alignItems="center"
            columns={4}
        >
            {list.map(el=> (
                <Grid item key={el.id}>
                    <Card key={el.id} sx={{ width: 345 }}>
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image={el.image}
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {el.title}
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    {el.text}
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Button size="small" color="primary">
                               {t("view")}
                            </Button>
                        </CardActions>
                    </Card>
                </Grid>
            )
            )}
        </Grid>
        </div>
    </div>
}

export default Noutati;