import { Card, CardActionArea, CardContent, CardMedia, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";
import './style.css';


const DespreNoi: React.FunctionComponent<{}> = () => {

  const { t } = useTranslation();
  return <div>
    <h1 style={{ textAlign: "center" }}>{t('about-us')}</h1>
    <Card sx={{ maxWidth: 1000 }} className="center">
      <CardActionArea>
        <CardMedia
          component="img"
          height="600"
          image="/images/logo.jpg"
          alt="green iguana"
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {t('agency')}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            <ul style={{ textAlign: "left" }}>
              <li>
                {t('about1')}
              </li>
              <li>
                {t('about2')}
              </li>
              <li>
                {t('about3')}
              </li>
              <li>
                {t('about4')}
              </li>
              <li>
                {t('about5')}
              </li>
            </ul>
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  </div>
}

export default DespreNoi;