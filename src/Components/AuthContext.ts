import { createContext, useContext } from "react";

export type GlobalContent = {
    isLoggedIn: boolean
    setIsLoggedIn:(l: boolean) => void
}

export const MyGlobalContext = createContext<GlobalContent>({
    isLoggedIn: false,
    setIsLoggedIn: () => {}
})

export const useGlobalContext = () => useContext(MyGlobalContext);
