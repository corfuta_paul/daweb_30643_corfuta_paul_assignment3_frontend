const xml_data = `<?xml version='1.0' encoding='utf-8'?>
<?xml-stylesheet type="text/xsl" href="ACSPIXMT.xsl" ?>

<News>
       <New id='1'>
           <Title>Casa viitorului</Title>
           <Text>Casa viitorului este aproape.Da click sa vezi cum o sa arate casa in care o sa locuim peste 10 ani.</Text>
           <Photo>/images/news1.jpg</Photo>
       </New>
       <New id='2'>
           <Title>Cea mai scumpa casuta</Title>
           <Text>Vezi cat de scumpa este aceasta casa de doar 40 de metrii patrati.</Text>
           <Photo>/images/news2.jpg</Photo>
       </New>
       <New id='3'>
           <Title>Renovam casele abandonate</Title>
           <Text>Afla mai multe detaliile despre viitorul nostru proiect!</Text>
           <Photo>/images/news3.jpg</Photo>
       </New>
</News>`



export default xml_data;

