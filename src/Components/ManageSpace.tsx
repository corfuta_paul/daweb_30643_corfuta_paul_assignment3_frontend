import { Modal } from "@material-ui/core";
import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardMedia, CircularProgress, Container, Grid, MenuItem, Stack, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Scheduler } from "@aldabil/react-scheduler";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import ActionTypes from "../Enums/ActionType";
import SpaceTypes from "../Enums/SpaceType";
import Space from "../Models/Space";
import SpaceService from "../Services/SpaceService";
import { useGlobalContext } from "./AuthContext";
import CanvasJSReact from './../canvasjs.react';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};


const events = [
    {
        event_id: 1,
        space_id: 2,
        title: "Paul Corfuta",
        start: new Date("2022/5/2 09:30"),
        end: new Date("2022/5/3 10:30"),
    },
    {
        event_id: 2,
        space_id: 2,
        title: "Maria Sima",
        start: new Date("2022/5/4 10:00"),
        end: new Date("2022/5/7 11:00"),
    },
    {
        event_id: 3,
        space_id: 6,
        title: "Madalina Stroe",
        start: new Date("2022/5/10 10:00"),
        end: new Date("2022/5/17 11:00"),
    },
    {
        event_id: 4,
        space_id: 6,
        title: "Popa Ioan",
        start: new Date("2022/5/18 10:00"),
        end: new Date("2022/5/20 11:00"),
    },
    {
        event_id: 5,
        space_id: 5,
        title: "Pop Marius",
        start: new Date("2022/5/4 10:00"),
        end: new Date("2022/5/10 11:00"),
    },
    {
        event_id: 6,
        space_id: 6,
        title: "Iacob Andrei",
        start: new Date("2022/5/4 10:00"),
        end: new Date("2022/5/7 11:00"),
    },
]

const options = {
    title: {
        text: "Chart"
    },
    data: [
        {
            // Change type to "doughnut", "line", "splineArea", etc.
            type: "column",
            dataPoints: [
                { label: "Monday", y: 0 },
                { label: "Tuesday", y: 0 },
                { label: "Wednsday", y: 0 },
                { label: "Thursday", y: 0 },
                { label: "Friday", y: 0 },
                { label: "Saturday", y: 0 },
                { label: "Sunday", y: 0 },
            ]
        }
    ]
}
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const spaceService: SpaceService = new SpaceService();

const ManageSpaces: React.FunctionComponent<{}> = () => {
    const { t } = useTranslation();
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const [isModalOpen2, setIsModalOpen2] = useState<boolean>(false);
    const [isModalOpen3, setIsModalOpen3] = useState<boolean>(false);
    const [spaces, setSpaces] = useState<Space[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [currentSpace, setCurrentSpace] = useState<Space>({ id: 0, name: "", dimension: 0, city: "", price: 0, photo: "", description: "", space_type: "", action_type: "" });
    const [isValid, setIsValid] = useState<boolean>(false);
    const [ev, setEv] = useState<any>([]);
    const [op, setOp] = useState<any>(options);
    var mon:number = 0;
    var tue:number = 0;
    var wed:number = 0;
    var thu:number = 0;
    var fri:number = 0;
    var sat:number = 0;
    var sun:number = 0;


    const onClickCancelHandler = () => {
        setIsModalOpen(false);
    }

    const onClickCancelHandler2 = () => {
        setIsModalOpen2(false);
    }

    const onClickCancelHandler3 = () => {
        setIsModalOpen3(false);
    }

    const onClickAddButton = () => {
        setIsModalOpen(true);
        setCurrentSpace({ id: 0, name: "", dimension: 0, city: "", price: 0, photo: "", description: "", space_type: "", action_type: "" });
    }

    const onClickEdit = (id: number) => {

        setCurrentSpace(spaces.find(q => q.id === id)!);
        setIsModalOpen(true);

    }

    const onClickViewApp = (id: number) => {

        setEv(events.filter(q => q.space_id === id)!);
        setIsModalOpen2(true);

    }

    const onClickViewStats = (id: number) => {

        events.forEach((ev)=>{
            if(ev.space_id===id){
                let dateOne = new Date(ev.start);
                let dateTwo = new Date(ev.end);
                for (var d = dateOne; d <= dateTwo; d.setDate(d.getDate() + 1)) {
                    var loopDay = new Date(d);
                    if(loopDay.getDay()===1){
                        mon++;
                    }
                    if(loopDay.getDay()===2){
                        tue++;
                    }
                    if(loopDay.getDay()===3){
                        wed++;
                    }
                    if(loopDay.getDay()===4){
                        thu++;
                    }
                    if(loopDay.getDay()===5){
                        fri++;
                    }
                    if(loopDay.getDay()===6){
                        sat++;
                    }
                    if(loopDay.getDay()===0){
                        sun++;
                    }
                }
            }
            setOp(prevState=>({
                ...prevState,
                data: [
                 {
                     type: "column",
                     dataPoints: [
                         { label: "Monday", y: mon },
                         { label: "Tuesday", y: tue },
                         { label: "Wednsday", y: wed },
                         { label: "Thursday", y: thu },
                         { label: "Friday", y: fri },
                         { label: "Saturday", y: sat },
                         { label: "Sunday", y: sun },
                     ]
                 }
             ]
            }))
        
        })
        setIsModalOpen3(true);

    }

    const onClickDelete = (id: number) => {

        spaceService.delete(id).then(() => {
            setSpaces(spaces.filter(q => q.id !== id)!);
        })

    }

    const onSubmitHandler = () => {

        if (currentSpace.id === 0) {
            spaceService.add(currentSpace).then((response) => {
                spaceService.getById(response).then((response) => {
                    setSpaces([...spaces, response]);
                    setIsModalOpen(false);
                })
            })
            return;
        }
        spaceService.update(currentSpace).then((response) => {
            let index: number = spaces.findIndex(q => q.id === response.id);
            spaces[index] = response;
            setSpaces(spaces);
            setIsModalOpen(false);
        })
    }

    const onNameChangeHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            name: event.target.value
        }))

    }

    const onDimensionChangeHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            dimension: event.target.valueAsNumber
        }))

    }

    const onPriceChangeHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            price: event.target.valueAsNumber
        }))

    }

    const onCityChangeHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            city: event.target.value
        }))

    }

    const onDescriptionChangeHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            description: event.target.value
        }))

    }

    const onSpaceTypeChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            space_type: event.target.value
        }))

    }

    const onActionTypeChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            action_type: event.target.value
        }))

    }

    const onPhotoChangeHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setCurrentSpace(prevState => ({
            ...prevState,
            photo: event.target.value
        }))

    }

    useEffect(() => {
        spaceService.getAll().then((response) => {
            setSpaces(response);
            setIsLoading(false);
        })
    }, [])

    useEffect(() => {
        if (currentSpace.name.length >= 2 &&
            currentSpace.city.length >= 2 &&
            currentSpace.dimension > 0 &&
            currentSpace.photo.length >= 2 &&
            currentSpace.price > 0 &&
            currentSpace.action_type !== "" &&
            currentSpace.space_type !== ""
        ) {
            setIsValid(true);
            return;
        }
        setIsValid(false);
    }, [currentSpace])

    return <div>
        {isLoading && <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: '400px' }}>
            <CircularProgress />
        </Box>}
        {!isLoading && <div>
            <Stack direction="row" spacing={1} justifyContent='center' alignContent='center' alignItems='center'>
                <h1>Manage Spaces</h1>
                <Button variant="contained" style={{ maxHeight: "50px" }} onClick={onClickAddButton}>
                    Add new space
                </Button>
            </Stack>
            <div style={{ padding: "40px 0px 40px 140px" }}>

                <Grid
                    container
                    direction="row"
                    rowSpacing={10}
                    columnSpacing={10}
                    alignItems="center"
                    columns={4}
                >
                    {spaces.map((space) => (
                        <Grid item key={space.id}>
                            <Card sx={{ width: 345 }} key={space.id}>
                                <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        height="345"
                                        image={`/images/${space.photo}`}
                                        alt="green iguana"
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div">
                                            {space.name}
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            <ul>
                                                <li>
                                                    {space.dimension} mp
                                                </li>
                                                <li>
                                                    {space.city}
                                                </li>
                                                <li>
                                                    {space.price} euro
                                                </li>
                                                <li>
                                                    3 {t('rooms')}
                                                </li>
                                            </ul>
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>

                                <Stack direction="row" spacing={1} justifyContent='center' style={{ paddingBottom: "15px" }}>
                                    <Button size="small" color="secondary" onClick={() => { onClickViewApp(space.id) }}>
                                        View Appointments
                                    </Button>
                                    <Button size="small" color="primary" onClick={() => { onClickEdit(space.id) }}>
                                        Edit
                                    </Button>
                                </Stack>
                                <Stack direction="row" spacing={1} justifyContent='center' style={{ paddingBottom: "15px" }}>
                                    <Button size="small" color="secondary" onClick={()=>{onClickViewStats(space.id)}}>
                                        View Statistics
                                    </Button>
                                    <Button size="small" color="primary" onClick={() => { onClickDelete(space.id) }}>
                                        Delete
                                    </Button>
                                </Stack>
                            </Card>
                        </Grid>
                    ))}


                </Grid>
            </div>
        </div>
        }
        <Modal
            open={isModalOpen}
            onClose={onClickCancelHandler}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableEnforceFocus
        >
            <Box sx={style}>
                {currentSpace.id === 0 && <Typography component="h1" variant="h5" textAlign='center'>
                    Add Space
                </Typography>}
                {currentSpace.id !== 0 && <Typography component="h1" variant="h5" textAlign='center'>
                    Edit Space
                </Typography>}
                <Box component="form" noValidate sx={{ mt: 3 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                name="name"
                                fullWidth
                                id="name"
                                label="Name"
                                onChange={onNameChangeHandler}
                                inputProps={{ minLength: 8 }}
                                value={currentSpace.name}
                                type={"name"}
                                variant="outlined"
                            />
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="city"
                                fullWidth
                                id="city"
                                label="City"
                                onChange={onCityChangeHandler}
                                inputProps={{ minLength: 8 }}
                                value={currentSpace.city}
                                type={"name"}
                                variant="outlined"
                            />
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="dimension"
                                fullWidth
                                id="dimension"
                                label="Dimension"
                                onChange={onDimensionChangeHandler}
                                inputProps={{ minLength: 8 }}
                                value={currentSpace.dimension}
                                type="number"
                                variant="outlined"
                            />
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="price"
                                fullWidth
                                id="price"
                                label="Price"
                                onChange={onPriceChangeHandler}
                                inputProps={{ minLength: 8 }}
                                value={currentSpace.price}
                                type="number"
                                variant="outlined"
                            />
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="photo"
                                fullWidth
                                id="photo"
                                label="Photo Name"
                                onChange={onPhotoChangeHandler}
                                inputProps={{ minLength: 8 }}
                                value={currentSpace.photo}
                                type="name"
                                variant="outlined"
                            />
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                name="description"
                                fullWidth
                                id="description"
                                label="Description"
                                onChange={onDescriptionChangeHandler}
                                inputProps={{ minLength: 8 }}
                                value={currentSpace.description}
                                type="text"
                                variant="outlined"
                            />
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="outlined-select-currency"
                                select
                                fullWidth
                                label="Space Type"
                                value={currentSpace.space_type}
                                onChange={onSpaceTypeChangeHandler}
                                helperText="Please select a space type"
                            >
                                {SpaceTypes.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                            <div className="errors">{""}</div>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                id="outlined-select-currency"
                                select
                                fullWidth
                                label="Action Type"
                                value={currentSpace.action_type}
                                onChange={onActionTypeChangeHandler}
                                helperText="Please select an action type"
                            >
                                {ActionTypes.map((option) => (
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                ))}
                            </TextField>
                            <div className="errors">{""}</div>
                        </Grid>

                    </Grid>

                    <Button
                        onClick={onSubmitHandler}
                        type="button"
                        fullWidth
                        variant="contained"
                        disabled={!isValid}
                        sx={{ mt: 3, mb: 2 }}
                    >
                        {currentSpace.id !== 0 && "Save Changes"}
                        {currentSpace.id === 0 && "Add Space"}
                    </Button>

                </Box>
            </Box>
        </Modal>
        <Modal
            open={isModalOpen2}
            onClose={onClickCancelHandler2}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableEnforceFocus>
            <Container maxWidth={"md"} style={{ backgroundColor: "coral", justifyContent: "center", alignContent: "center" }}>
                <Scheduler
                    view="month"
                    events={ev}
                />
            </Container>
        </Modal>
        <Modal
            open={isModalOpen3}
            onClose={onClickCancelHandler3}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableEnforceFocus>
            <Container maxWidth={"md"} style={{ backgroundColor: "coral" }}>
                <CanvasJSChart options={op}/>
            </Container>
        </Modal>

    </div >
}


export default ManageSpaces;