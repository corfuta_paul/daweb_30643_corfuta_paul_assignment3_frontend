import React, { useEffect, useState } from 'react';
import { Button, CssBaseline, Box, Typography, Grid, TextField, Container, Alert } from "@mui/material";
import Link from '@mui/material/Link';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import './style.css';
import { Navigate, useNavigate } from 'react-router-dom';
import { Snackbar } from '@material-ui/core';
import User from '../Models/User';
import UserService from '../Services/UserService';


const theme = createTheme();
const userService: UserService = new UserService();

const RegisterForm: React.FunctionComponent<{}> = () => {

    const [user, setUser] = useState<User>({ id: 0, name: "", password: "", email: "", is_agent:0 });
    const navigate = useNavigate();
    const [confirmPassword, setConfirmPassword] = useState<string>("");
    const [nameError, setNameError] = useState('');
    const [emailError, setEmailError] = useState('');
    const [passwordError, setPasswordError] = useState('');
    const [confirmPasswordError, setConfirmPasswordError] = useState('');
    const [isValid, setIsValid] = useState<boolean>(false);
    const [registerError, setRegisterError] = useState<string>("");

    useEffect(() => {
        if (user.name.length >= 2 &&
            user.password.trim() !== "" &&
            user.password.length >= 8 &&
            confirmPassword === user.password) {
            setIsValid(true);
            return;
        }
        setIsValid(false);
    }, [user, confirmPassword])

    const onChangeEmailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            email: event.target.value
        }));
        const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!event.target.value || regex.test(event.target.value) === false) {
            setEmailError('Email is not valid');
        } else {
            setEmailError('');
        }

    }

    const onChangePasswordHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            password: event.target.value
        }));
        if (event.target.value.length < 8) {
            setPasswordError('Password should have at least 8 characters');
        } else {
            setPasswordError('');
        }
    }

    const onChangeNameHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            name: event.target.value
        }));
        if (event.target.value.length < 2) {
            setNameError('First name should have at least 2 characters')
        } else {
            setNameError('');
        }
    }

    const onChangeConfirmPasswordHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setConfirmPassword(event.target.value);
        if (event.target.value !== user.password) {
            setConfirmPasswordError("Passwords don't match");
        } else {
            setConfirmPasswordError("");
        }
    }


    const handleSubmit = () => {
        userService.register(user).then(() => {
            navigate('/home');
        })
    };

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setRegisterError('');
    };

    return (
        <div>
            <ThemeProvider theme={theme}>
                <Container className={"form"} component="main" maxWidth="xs">
                    <CssBaseline />
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Typography component="h1" variant="h5" style={{ paddingTop: 20 }}>
                            Sign up
                        </Typography>
                        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        name="name"
                                        required
                                        fullWidth
                                        id="name"
                                        label="Name"
                                        onChange={onChangeNameHandler}
                                        inputProps={{ minLength: 2 }}
                                        autoFocus
                                    />
                                    <div className="errors">{nameError}</div>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        id="email"
                                        label="Email Address"
                                        name="email"
                                        autoComplete="email"
                                        onChange={onChangeEmailHandler}
                                    />
                                    <div className="errors">{emailError}</div>

                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="new-password"
                                        onChange={onChangePasswordHandler}
                                    />
                                    <div className="errors">{passwordError}</div>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        name="confirmPassword"
                                        label="Confirm password"
                                        type="password"
                                        id="confirmPassword"
                                        autoComplete="new-password"
                                        onChange={onChangeConfirmPasswordHandler}
                                    />
                                    <div className="errors">{confirmPasswordError}</div>
                                </Grid>
                            </Grid>
                            <Button
                                onClick={() => { handleSubmit() }}
                                type="button"
                                fullWidth
                                variant="contained"
                                disabled={!isValid}
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Sign Up
                            </Button>
                            <Grid container justifyContent="flex-end">
                                <Grid item style={{ paddingBottom: 20 }}>
                                    <Link href="/login" variant="body2">
                                        Already have an account? Sign in
                                    </Link>
                                </Grid>
                            </Grid>
                        </Box>
                    </Box>
                </Container>
            </ThemeProvider>
            <Snackbar
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                open={registerError != ''}
                autoHideDuration={6000}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {registerError}
                </Alert>
            </Snackbar>
        </div>
    );
}

export default RegisterForm;
