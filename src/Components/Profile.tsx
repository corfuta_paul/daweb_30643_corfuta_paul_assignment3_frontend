import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import Avatar from '@mui/material/Avatar';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import { Alert, Button, Checkbox, Chip, CircularProgress, Container, Dialog, FormControl, InputLabel, List, ListItem, ListItemText, MenuItem, OutlinedInput, Select, SelectChangeEvent, Snackbar, Stack } from '@mui/material';
import { useEffect, useState } from 'react';
import './style.css';
import { Grid, Modal, TextField } from '@material-ui/core';
import UserService from '../Services/UserService';
import User from '../Models/User';
import PreferredType from '../Models/PreferredType';
import PreferredTypeService from '../Services/PreferredTypeService';
import UserPreferredType from '../Models/UserPreferredType';
import UserPreferredTypeService from '../Services/UserPreferredTypeService';
import { styled } from '@mui/material/styles';
import FileService from '../Services/FileService';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Input = styled('input')({
    display: 'none',
});

const userService: UserService = new UserService();
const preferredTypeService: PreferredTypeService = new PreferredTypeService();
const userPreferredTypeService: UserPreferredTypeService = new UserPreferredTypeService();
const fileService: FileService = new FileService();

const Profile: React.FunctionComponent<{}> = () => {

    const [user, setUser] = useState<User>({ id: 0, name: "", email: "", password: "", is_agent: 0 });
    const [pref, setPref] = useState<PreferredType[]>([]);
    const [userPref, setUserPref] = useState<UserPreferredType[]>([]);
    const [newUser, setNewUser] = useState<User>({ id: 0, name: "", email: "", password: "", is_agent: 0 });
    const [isLoading, setIsLoading] = useState<boolean>(true);
    const [isOpened, setIsOpened] = useState<boolean>(false);
    const [isValid, setIsValid] = useState<boolean>(false);
    const [nameError, setNameError] = useState<string>("");
    const [personName, setPersonName] = useState<string[]>([]);
    const [selectedFile, setSelectedFile] = useState<Blob>(new Blob(["GeeksForGeeks"],
        { type: "text/plain" }));
    const [uploadMessage, setUploadMessage] = useState<string>("");
    const [selected, setSelected] = useState<boolean>(false);

    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;
    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    useEffect(() => {
        if (newUser.name.length > 2) {
            setIsValid(true);
            return;
        }
        setIsValid(false);
    }, [newUser])

    useEffect(() => {
        setUser(JSON.parse(sessionStorage.getItem("User")!));
        preferredTypeService.getAll().then((response) => {
            setPref(response);
        })
    }, [])

    useEffect(() => {
        userPreferredTypeService.getByUserId(user.id).then((response) => {
            setUserPref(response);
            setIsLoading(false);
        })
    }, [pref])

    useEffect(() => {
        setNewUser({ id: user.id, name: user.name, email: user.email, password: user.password, is_agent: user.is_agent });
    }, [user])


    const handleClose = () => {
        setIsOpened(false);
        setPersonName([]);
    }

    const handleCloseSnack = () => {
        setUploadMessage("");
    }

    const handleOpen = () => {
        setIsOpened(true);
        setNameError("");
    }

    const onChangeNameHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setNewUser(prevState => ({
            ...prevState,
            name: event.target.value
        }));
        if (event.target.value.length <= 2) {
            setNameError('Name must be longer');
        } else {
            setNameError('');
        }
    }

    const onEdit = () => {
        userService.edit(newUser).then((response) => {
            setUser(prevState => ({
                ...prevState,
                name: newUser.name
            }));
        })
        var list:UserPreferredType[] = [];
        personName.forEach((el) => {
            const id = pref.find(q => q.name === el)?.id;
            const p = userPref.find(q => q.preferred_type_id === id && q.user_id === user.id);
            if (p == null) {
                const prefType: UserPreferredType = { id: 0, user_id: user.id, preferred_type_id: id as number };
                userPreferredTypeService.add(prefType).then((response) => {
                    list = [...list,response];
                    setUserPref([...userPref, response]);
                })
            }
        })
        setPersonName([]);
        setIsOpened(false);
    }

    const handleChange = (event: SelectChangeEvent<typeof personName>) => {
        const {
            target: { value },
        } = event;
        setPersonName(
            typeof value === 'string' ? value.split(',') : value,
        );
    };

    const onDeleteHandler = (id: number) => {
        setIsLoading(true);
        userPreferredTypeService.delete(id).then(() => {
            setUserPref(userPref.filter(q => q.id !== id));
            setIsLoading(false);
        })
    }

    const handleSubmit = (event) => {
        event.preventDefault()
        const formData = new FormData();
        formData.append("file", selectedFile);
        if (selected != false) {
            fileService.add(formData, user.id).then((response) => {
                setUploadMessage("Successfully uploaded!");
            }).catch(() => {
                setUploadMessage("Something went wrong!");
            })
            return;
        }
        setUploadMessage("You have to select a file!")

    }

    const handleFileSelect = (event) => {
        setSelectedFile(event.target.files[0])
        setSelected(true);
    }

    return (<div className='profile-card'>
        {isLoading && <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: '400px' }}>
            <CircularProgress />
        </Box>}
        {!isLoading && <Container component="main" maxWidth="xs" >
            <CssBaseline />
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    maxHeight: '700px',
                    maxWidth: '700px'
                }}
            >
                <Card sx={{ width: 500 }}>
                    <CardHeader
                        avatar={
                            <Avatar sx={{ bgcolor: red[500] }} >
                                {user.name[0]}
                            </Avatar>
                        }
                        title={`${user.name}`}
                        subheader={user.email}
                    />

                    <CardContent>
                        <Typography variant="button" display="block" gutterBottom>
                            Preferences:
                        </Typography>
                        <Stack direction="row" spacing={1}>
                            {userPref.map((pr) => (
                                <Chip key={pr.id} label={pref.find(q => q.id === pr.preferred_type_id)?.name} variant="outlined" onDelete={() => { onDeleteHandler(pr.id) }} />
                            ))}
                        </Stack>
                    </CardContent>
                    <CardContent>
                        <Typography variant="button" display="block" gutterBottom>
                            Upload a GDPR file:
                        </Typography>
                        <form onSubmit={handleSubmit}>
                            <input type="file" onChange={handleFileSelect} />
                            <input type="submit" value="Upload File" />
                        </form>
                    </CardContent>
                    <Button sx={{ width: 500 }} color="primary" onClick={() => handleOpen()}>
                        Edit
                    </Button>
                </Card>

            </Box>
        </Container>}
        <Modal
            open={isOpened}
            onClose={handleClose}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableEnforceFocus
        >
            <Box sx={style}>
                <Typography component="h1" variant="h5" >
                    Edit User
                </Typography>
                <Box component="form" noValidate sx={{ mt: 3 }}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                name="name"
                                fullWidth
                                id="name"
                                label="Name"
                                onChange={onChangeNameHandler}
                                inputProps={{ minLength: 8 }}
                                autoFocus
                                value={newUser.name}
                                type={"name"}
                                variant="outlined"
                            />
                            <div className="errors">{nameError}</div>
                        </Grid>
                        <Grid >
                            <FormControl sx={{ m: 1, width: 332 }} >
                                <InputLabel id="demo-multiple-checkbox-label" >Preferences</InputLabel>

                                <Select
                                    labelId="demo-multiple-checkbox-label"
                                    id="demo-multiple-checkbox"
                                    multiple
                                    value={personName}
                                    onChange={handleChange}
                                    input={<OutlinedInput label="Preferences" />}
                                    renderValue={(selected) => selected.join(', ')}
                                    MenuProps={MenuProps}
                                >
                                    {pref.map((pr) => (
                                        <MenuItem key={pr.id} value={pr.name}>
                                            <Checkbox checked={personName.indexOf(pr.name) > -1} />
                                            <ListItemText primary={pr.name} />
                                        </MenuItem>
                                    ))}
                                </Select>

                            </FormControl>
                        </Grid>
                    </Grid>
                    <Button
                        onClick={onEdit}
                        type="button"
                        fullWidth
                        variant="contained"
                        disabled={!isValid}
                        sx={{ mt: 3, mb: 2 }}
                    >
                        Save Changes
                    </Button>
                </Box>
            </Box>
        </Modal>
        <Snackbar
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
            }}
            open={uploadMessage === 'Successfully uploaded!'}
            autoHideDuration={6000}
            onClose={handleCloseSnack}>
            <Alert onClose={handleCloseSnack} severity="success" sx={{ width: '100%' }}>
                {uploadMessage}
            </Alert>
        </Snackbar>
        <Snackbar
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
            }}
            open={uploadMessage === 'Something went wrong!'}
            autoHideDuration={6000}
            onClose={handleCloseSnack}>
            <Alert onClose={handleCloseSnack} severity="error" sx={{ width: '100%' }}>
                {uploadMessage}
            </Alert>
        </Snackbar>
        <Snackbar
            anchorOrigin={{
                vertical: "bottom",
                horizontal: "center"
            }}
            open={uploadMessage === 'You have to select a file!'}
            autoHideDuration={6000}
            onClose={handleCloseSnack}>
            <Alert onClose={handleCloseSnack} severity="warning" sx={{ width: '100%' }}>
                {uploadMessage}
            </Alert>
        </Snackbar>
    </div >)
}

export default Profile;