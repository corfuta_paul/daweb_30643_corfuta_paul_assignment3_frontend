import React, { useEffect, useState } from 'react';
import { Button, CssBaseline, Box, Typography, Grid, TextField, Container, Snackbar, Alert } from "@mui/material";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import './style.css';
import UserService from '../Services/UserService';
import { useNavigate } from 'react-router-dom';
import User from '../Models/User';
import { useGlobalContext } from './AuthContext';

const theme = createTheme();

const LoginForm: React.FunctionComponent<{}> = () => {

    const { isLoggedIn, setIsLoggedIn } = useGlobalContext();
    const [user, setUser] = useState<User>({ email: "", password: "", name:"", id:0, is_agent: 0 });
    const [emailError, setEmailError] = useState<string>('');
    const [passwordError, setPasswordError] = useState<string>('');
    const [isValid, setIsValid] = useState<boolean>(false);
    const userService: UserService = new UserService();
    const [loginError, setLoginError] = React.useState<string>('');
    const navigate = useNavigate();

    useEffect(() => {
        if (user.email.trim() !== "" &&
            user.password.trim() !== "" &&
            user.password.length >= 8) {
            setIsValid(true);
            return;
        }
        setIsValid(false);
    }, [user])

    const onChangeEmailHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            email: event.target.value
        }))
        const regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!event.target.value || regex.test(event.target.value) === false) {
            setEmailError('Email is not valid');
        } else {
            setEmailError('');
        }
    }

    const onChangePasswordHandler = (event: React.FocusEvent<HTMLInputElement>) => {
        setUser(prevState => ({
            ...prevState,
            password: event.target.value
        }))
        if (event.target.value.length < 8) {
            setPasswordError('Password should have at least 8 characters');
        } else {
            setPasswordError('');
        }
    }

    const handleClose = (event?: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }
        setLoginError('');
    };

    const handleSubmit = () => {
        userService.login(user).then((response) => {
            if(response.email!=="error")
            {
                sessionStorage.setItem("User", JSON.stringify(response));
                setIsLoggedIn(true);
                navigate("/profile");  
                return;
            }
            setLoginError("Wrong Credentials!");
        })
    };

    return (
        <div>
            <ThemeProvider theme={theme}>
                <Container className={"form"} component="main" maxWidth="xs">
                    <CssBaseline />
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <Typography component="h1" variant="h5" style={{ paddingTop: 20 }}>
                            Login
                        </Typography>
                        <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }} style={{ paddingBottom: 10 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        name="emailaddress"
                                        fullWidth
                                        id="Email Address"
                                        label="Email Address"
                                        onChange={onChangeEmailHandler}
                                    />
                                    <div className="errors">{emailError}</div>
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        id="password"
                                        label="Password"
                                        onChange={onChangePasswordHandler}
                                        name="password"
                                        type={"password"}
                                    />
                                    <div className="errors">{passwordError}</div>
                                </Grid>
                            </Grid>
                            <Button
                                onClick={() => { handleSubmit() }}
                                type="button"
                                fullWidth
                                variant="contained"
                                disabled={!isValid}
                                sx={{ mt: 3, mb: 2 }}
                            >
                                Log in
                            </Button>
                        </Box>
                    </Box>
                </Container>
            </ThemeProvider>
            <Snackbar
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "center"
                }}
                open={loginError != ''}
                autoHideDuration={6000}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity="error" sx={{ width: '100%' }}>
                    {loginError}
                </Alert>
            </Snackbar>
        </div>
    );
}

export default LoginForm;
