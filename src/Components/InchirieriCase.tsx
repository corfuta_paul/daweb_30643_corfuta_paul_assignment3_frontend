import { Modal } from "@material-ui/core";
import { Box, Button, Card, CardActionArea, CardActions, CardContent, CardMedia, CircularProgress, Grid, Stack, Typography } from "@mui/material";
import { useEffect, useState } from "react";

import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import Space from "../Models/Space";
import SpaceService from "../Services/SpaceService";
import { useGlobalContext } from "./AuthContext";

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const spaceService:SpaceService = new SpaceService();

const InchirieriCase: React.FunctionComponent<{}> = () => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const { isLoggedIn, setIsLoggedIn } = useGlobalContext();
    const [isModalOpen,setIsModalOpen] = useState<boolean>(false);
    const [isModalOpen2,setIsModalOpen2] = useState<boolean>(false);
    const [spaces,setSpaces] = useState<Space[]>([]);
    const [isLoading,setIsLoading] = useState<boolean>(true);


    const onClickRentHandler = () => {
        if (isLoggedIn === true) {
           setIsModalOpen2(true);
           return;
        }
        setIsModalOpen(true);
        
    }

    const onClickLoginHandler = () => {
        navigate('/login');
    }

    const onClickCancelHandler = () => {
        setIsModalOpen(false);
    }

    useEffect(()=>{
        spaceService.getAll().then((response)=>{
            setSpaces(response.filter(q=>q.space_type==="House" && q.action_type==="Rent")!);
            setIsLoading(false);
        })
    },[])

    return <div>
        {isLoading && <Box sx={{ display: 'flex', justifyContent: 'center', paddingTop: '400px' }}>
            <CircularProgress />
        </Box>}
        {!isLoading && <div>
        <h1 style={{ textAlign: "center" }}>{t('houses')}</h1>
        <div style={{ padding: "40px 0px 40px 140px" }}>

            <Grid
                container
                direction="row"
                rowSpacing={10}
                columnSpacing={10}
                alignItems="center"
                columns={4}
            >
              {spaces.map((space)=>(
                    <Grid item key={space.id}>
                    <Card sx={{ width: 345 }} >
                        <CardActionArea>
                            <CardMedia
                                component="img"
                                height="345"
                                image={`/images/${space.photo}`}
                                alt="green iguana"
                            />
                            <CardContent>
                                <Typography gutterBottom variant="h5" component="div">
                                    {space.name}
                                </Typography>
                                <Typography variant="body2" color="text.secondary">
                                    <ul>
                                        <li>
                                            {space.dimension} mp
                                        </li>
                                        <li>
                                            {space.city}
                                        </li>
                                        <li>
                                            {space.price} euro
                                        </li>
                                        <li>
                                            3 {t('rooms')}
                                        </li>
                                    </ul>
                                </Typography>
                            </CardContent>
                        </CardActionArea>
                        <CardActions>
                            <Stack direction="row" spacing={1}>
                                <Button size="small" color="primary">
                                    {t('view')}
                                </Button>
                                <Button size="small" color="primary" onClick={onClickRentHandler}>
                                    Rent
                                </Button>
                            </Stack>

                        </CardActions>
                    </Card>
                </Grid>
              ))}
               

            </Grid>
        </div>
        </div>}
        <Modal
            open={isModalOpen}
            onClose={() => {setIsModalOpen(false)}}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableEnforceFocus
        >
            <Box sx={style} justifyContent='center'>
                <Typography component="h1" variant="h5" textAlign='center'>
                    To continue you have to log in!
                </Typography>
                <Box component="form" noValidate sx={{ mt: 3 }}>
                    <Stack direction="row" spacing={1} alignContent='center' justifyContent='center'>
                        <Button size="small" color="primary" onClick={onClickLoginHandler}>
                            Log In
                        </Button>
                        <Button size="small" color="primary" onClick={onClickCancelHandler}>
                            Close
                        </Button>
                    </Stack>
                </Box>
            </Box>
        </Modal>
        <Modal
            open={isModalOpen2}
            onClose={() => {setIsModalOpen2(false)}}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
            disableEnforceFocus
        >
            <Box sx={style} justifyContent='center'>
                <Typography component="h1" variant="h5" textAlign='center'>
                    Now you can rent :)
                </Typography>
            </Box>
        </Modal>

    </div>
}


export default InchirieriCase;