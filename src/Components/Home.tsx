import './style.css';
import { useTranslation } from 'react-i18next';


const Home:React.FunctionComponent<{}> = () =>{
    const {t} = useTranslation();
    return <div>
    <h1 style={{textAlign:"center"}}>{t('home')}</h1>
    <img className={"picture"} src="images/home.jpg" style={{borderRadius:"1000"!}} />
    </div> 
}

export default Home;
