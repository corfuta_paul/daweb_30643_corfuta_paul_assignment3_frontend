interface UserPreferredType {
    id:number,
    user_id:number,
    preferred_type_id:number
}

export default UserPreferredType;