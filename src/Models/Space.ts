interface Space {
    id:number,
    name:string,
    dimension:number,
    city:string,
    price:number,
    photo:string,
    description:string,
    action_type:string,
    space_type:string
}

export default Space;