interface User {
     id:number,
     name:string,
     email:string,
     password:string,
     is_agent:number
}

export default User;