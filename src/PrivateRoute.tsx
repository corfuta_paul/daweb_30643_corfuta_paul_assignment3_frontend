import { Navigate, Outlet, useLocation } from "react-router-dom";

const CustomWrapper = ({ isLoggedIn, ...props }) => {
    const location = useLocation();
    return isLoggedIn? (
      <Outlet />
    ) : (
      <Navigate
        to={`/login/${location.search}`}
        replace
        state={{ location }}
      />
    )
  };

export default CustomWrapper;