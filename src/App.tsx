import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './Components/Home';
import AppMenu from './Components/AppMenu';
import Noutati from './Components/Noutati';
import DespreNoi from './Components/DespreNoi';
import InchirieriApartamente from './Components/InchirieriApartamente';
import Contact from './Components/Contact';
import { makeStyles } from "@material-ui/core/styles"
import InchirieriCase from './Components/InchirieriCase';
import InchirieriGarsoniere from './Components/InchirieriGarsoniere';
import VanzariApartamente from './Components/VanzariApartamente';
import VanzariCase from './Components/VanzariCase';
import VanzariGarsoniere from './Components/VanzariGarsoniere';
import { Suspense, useState } from 'react';
import LoginForm from './Components/LoginForm';
import RegisterForm from './Components/RegisterForm';
import { MyGlobalContext } from './Components/AuthContext';
import Profile from './Components/Profile';
import ManageSpaces from './Components/ManageSpace';
import CustomWrapper from './PrivateRoute';

const useStyles = makeStyles((theme) => ({
  root: {
    minHeight: "100vh",
    backgroundImage: `url(${process.env.PUBLIC_URL + "/images/background.jpg"})`
  }
}))
function App() {
  const classes = useStyles();
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  return (
    <div className={classes.root}>
      <MyGlobalContext.Provider value={{ isLoggedIn, setIsLoggedIn }}>
        <BrowserRouter>
          <AppMenu />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/home" element={<Home />} />
            <Route path="/noutati" element={<Noutati />} />
            <Route path="/despre-noi" element={<DespreNoi />} />
            <Route path="/inchirieri/apartamente" element={<InchirieriApartamente />} />
            <Route path="/inchirieri/case" element={<InchirieriCase />} />
            <Route path="/inchirieri/garsoniere" element={<InchirieriGarsoniere />} />
            <Route path="/vanzari/apartamente" element={<VanzariApartamente />} />
            <Route path="/vanzari/case" element={<VanzariCase />} />
            <Route path="/vanzari/garsoniere" element={<VanzariGarsoniere />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/login" element={<LoginForm />} />
            <Route path="/register" element={<RegisterForm />} />
            <Route path="/profile" element={<CustomWrapper isLoggedIn={isLoggedIn} />} >
              <Route path="/profile" element={<Profile />} />
            </Route>
            <Route path="/manage" element={<CustomWrapper isLoggedIn={isLoggedIn} />} >
             <Route path="/manage" element={<ManageSpaces />} />
            </Route> 
          </Routes>
        </BrowserRouter>
      </MyGlobalContext.Provider>
    </div>
  );
}

export default App;
