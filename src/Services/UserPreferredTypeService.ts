import axios from "axios";
import {HOST} from "../Host";
import UserPreferredType from "../Models/UserPreferredType";

class UserPreferredTypeService {

    add(pref: UserPreferredType): Promise<UserPreferredType> {
        return axios.post(HOST.backend_api+'userpreferredtype/add',pref).then((response)=>{
              return response.data;
        })
    }

    getByUserId(id:number): Promise<UserPreferredType[]> {
        return axios.get(HOST.backend_api+`userpreferredtype/getbyuserid/${id}`).then((response)=>{
              return response.data;
        })
    }

    delete(id:number): Promise<void> {
        return axios.delete(HOST.backend_api+`userpreferredtype/delete/${id}`).then((response)=>{
              return response.data;
        })
    }

}

export default UserPreferredTypeService;