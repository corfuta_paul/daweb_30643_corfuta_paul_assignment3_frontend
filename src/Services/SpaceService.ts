import axios from "axios";
import {HOST} from "../Host";
import Space from "../Models/Space";


class SpaceService {

    getAll(): Promise<Space[]> {
        return axios.get(HOST.backend_api+'space/get').then((response)=>{
              return response.data;
        })
    }

    getById(id:number): Promise<Space> {
        return axios.get(HOST.backend_api+`space/get/${id}`).then((response)=>{
              return response.data;
        })
    }

    add(space: Space): Promise<number> {
        return axios.post(HOST.backend_api+'space/add',space).then((response)=>{
            return response.data;
      })
    }

    update(space: Space): Promise<Space> {
        return axios.put(HOST.backend_api+'space/edit',space).then((response)=>{
            return response.data;
      })
    }

    delete(id:number): Promise<void> {
        return axios.delete(HOST.backend_api+`space/delete/${id}`).then((response)=>{
              return response.data;
        })
    }
}

export default SpaceService;