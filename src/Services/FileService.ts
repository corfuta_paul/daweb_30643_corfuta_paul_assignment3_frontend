import axios from "axios";
import {HOST} from "../Host";
import File from "../Models/File";

class FileService {

    add(file:FormData, id:number): Promise<string> {
        return axios.post(`${HOST.backend_api}file/${id}`,file).then((response)=>{
              return response.data;
        })
    }

}

export default FileService;