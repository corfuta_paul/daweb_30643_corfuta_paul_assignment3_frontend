import axios from "axios";
import {HOST} from "../Host";
import PreferredType from "../Models/PreferredType";

class PreferredTypeService {

    add(pref: PreferredType): Promise<string> {
        return axios.post(HOST.backend_api+'preferredtype/add',pref).then((response)=>{
              return response.data;
        })
    }

    getAll(): Promise<PreferredType[]> {
        return axios.get(HOST.backend_api+'preferredtype/get').then((response)=>{
              return response.data;
        })
    }
}

export default PreferredTypeService;