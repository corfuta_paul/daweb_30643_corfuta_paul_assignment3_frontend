import axios from "axios";
import User from "../Models/User";
import {HOST} from "../Host";

class UserService {
    register(user: User): Promise<string> {
        return axios.post(HOST.backend_api+'register/',user).then((response)=>{
              return response.data;
        })
    }

    login(user: User): Promise<User> {
        return axios.post(HOST.backend_api+'login/',user).then((response)=>{
              return response.data;
        })
    }

    edit(user: User): Promise<User> {
        return axios.put(HOST.backend_api+'user/edit',user).then((response)=>{
              return response.data;
        })
    }
}

export default UserService;